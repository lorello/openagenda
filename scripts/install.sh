#!/usr/bin/env bash

set -e

if [[ -z $EZ_ROOT ]]; then
    echo "[error] EZ_ROOT is empty this variable is required in this container, please set it to the public dir of Ez and restart"
    exit 1
else
    echo "[info] Current root is ${EZ_ROOT}"
fi

RUN_INSTALLER=${RUN_INSTALLER:-'true'}
if [[ -f vendor/bin/ocinstall ]]; then
	if [[ $RUN_INSTALLER == 'true' ]]; then
        if [[ -n $EZ_INSTANCE ]]; then
            echo "[info] run installer on ${EZ_INSTANCE}"
            php vendor/bin/ocinstall --allow-root-user -s${EZ_INSTANCE}_backend --embed-dfs-schema --no-interaction ../installer/
        fi
    else
        echo "[info] RUN_INSTALLER is set to false"
    fi
else
    echo "[warning] Installer bin vendor/bin/ocinstall not found"
fi

exec "$@"