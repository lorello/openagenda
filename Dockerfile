FROM opencontentcoop/ezpublish:0.0.5

WORKDIR /var/www

COPY composer.json composer.lock /var/www/

RUN echo "Running composer"  \
	&& composer global require hirak/prestissimo \
	&& COMPOSER_ALLOW_SUPERUSER=1 composer install --prefer-dist --no-scripts --no-dev \ 
	&& rm -rf /root/.composer \
	&& ls -l html/vendor/bin


# Add some custom config... only if strictly needed for a single instance
# otherwise change it in the base-image
# COPY conf.d/php.ini ${PHP_INI_DIR}/conf.d/php.ini

# Add custom settings
COPY conf.d/ez /var/www/html/settings

# Add installer
COPY conf.d/installer /var/www/installer

COPY security.txt /var/www/html/.well-known/security.txt

# Copy install script to default entrypoint script dir to extend base entrypoint without modifying it
# @see https://github.com/OpencontentCoop/docker-ezpublish/blob/master/scripts/docker-entrypoint.sh#L84
COPY scripts/install.sh /docker-entrypoint-initdb.d/

WORKDIR /var/www/html

RUN php bin/php/ezcache.php --clear-id=global_ini --allow-root-user \
    && php bin/php/ezpgenerateautoloads.php -e


# The following directives are already present in the base-image
# don't change them here unless for debugging or improvement of the
# base-image itself.
#ENTRYPOINT ["/scripts/docker-entrypoint.sh"]
#CMD php-fpm
#EXPOSE 9000
