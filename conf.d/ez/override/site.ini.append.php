<?php /* #?ini charset="utf-8"?

[VarnishSettings]
VarnishHostName=varnish
VarnishPort=8080

[HTTPHeaderSettings]
CustomHeader=enabled

[ExpiryHandler]
ExpiryFilePerSiteAccess=enabled

[SearchSettings]
LogSearchStats=disabled

[RoleSettings]
PolicyOmitList[]=user/do_logout
PolicyOmitList[]=exportas/csv
PolicyOmitList[]=exportas/custom
PolicyOmitList[]=ezinfo/is_alive
PolicyOmitList[]=opendata/api

[Session]
SessionNameHandler=custom
#Set to true if https
CookieSecure=true
CookieHttponly=true
Handler=ezpSessionHandlerPHP
ForceStart=disabled

[SiteSettings]
SiteList[]=prototipo_backend
SiteList[]=prototipo_agenda
SiteList[]=prototipo_ita_agenda
#SiteList


[UserSettings]
LogoutRedirect=/?logout

[SiteAccessSettings]
ForceVirtualHost=true
DebugAccess=enabled
DebugExtraAccess=enabled
CheckValidity=false
MatchOrder=host_uri
AvailableSiteAccessList[]=prototipo_backend
AvailableSiteAccessList[]=prototipo_agenda
AvailableSiteAccessList[]=prototipo_ita_agenda
#AvailableSiteAccessList

HostUriMatchMapItems[]=openagenda.local;backend;prototipo_backend
HostUriMatchMapItems[]=openagenda.local;it;prototipo_ita_agenda
HostUriMatchMapItems[]=openagenda.local;;prototipo_agenda
HostUriMatchMapItems[]=openagenda.localtest.me;backend;prototipo_backend
HostUriMatchMapItems[]=openagenda.localtest.me;it;prototipo_ita_agenda
HostUriMatchMapItems[]=openagenda.localtest.me;;prototipo_agenda

#HostUriMatchMapItems

#TempHostUriMatchMapItems


[MailSettings]
AdminEmail=no-reply@opencontent.it
EmailSender=no-reply@opencontent.it
TransportAlias[smtp]=OpenPASMTPTransport
Transport=smtp
TransportConnectionType=
TransportPort=1025
TransportServer=mailhog
TransportUser=
TransportPassword=

BlackListEmailDomains[]
BlackListEmailDomainSuffixes[]
BlackListEmailDomainSuffixes[]=ru

[EmbedViewModeSettings]
AvailableViewModes[]=embed
AvailableViewModes[]=embed-inline
InlineViewModes[]=embed-inline

[TimeZoneSettings]
TimeZone=Europe/Rome

[RegionalSettings]
TextTranslation=enabled

[ContentSettings]
ContentObjectNameLimit=203
TranslationList=

[DebugSettings]
DebugToolbar=disabled

[UserFormToken]
CookieHttponly=true
CookieSecure=1


*/ ?>
