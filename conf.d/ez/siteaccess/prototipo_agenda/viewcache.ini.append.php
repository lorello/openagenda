<?php /* #?ini charset="utf-8"?

[ViewCacheSettings]
ClearRelationTypes[]
ClearRelationTypes[]=common
ClearRelationTypes[]=reverse_common
ClearRelationTypes[]=reverse_embedded
SmartCacheClear=enabled

[programma_eventi]
DependentClassIdentifier[]=agenda_root
ClearCacheMethod[]=object
ClearCacheMethod[]=parent

[event]
DependentClassIdentifier[]=event_calendar
ClearCacheMethod[]=object
ClearCacheMethod[]=parent

[private_organization]
DependentClassIdentifier[]=folder
ClearCacheMethod[]=object
ClearCacheMethod[]=parent
ClearCacheMethod[]=relating

*/ ?>