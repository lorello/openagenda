<?php /* #?ini charset="utf-8"?

[HTTPHeaderSettings]
CustomHeader=enabled
OnlyForAnonymous=disabled
OnlyForContent=enabled
Cache-Control[]
Cache-Control[/]=public, must-revalidate, max-age=259200, s-maxage=259200
HeaderList[]=Vary
Vary[/]=X-User-Context-Hash
HeaderList[]=X-Instance
X-Instance[/]=prototipo

[ExtensionSettings]
ActiveAccessExtensions[]=ezflow
ActiveAccessExtensions[]=ezgmaplocation
ActiveAccessExtensions[]=ezjscore
ActiveAccessExtensions[]=ezmultiupload
ActiveAccessExtensions[]=ezodf
ActiveAccessExtensions[]=ezoe
ActiveAccessExtensions[]=ezwt
ActiveAccessExtensions[]=sqliimport
ActiveAccessExtensions[]=openpa
ActiveAccessExtensions[]=ezfind
ActiveAccessExtensions[]=ocsearchtools
ActiveAccessExtensions[]=ezprestapiprovider
ActiveAccessExtensions[]=ocopendata
ActiveAccessExtensions[]=ocexportas
ActiveAccessExtensions[]=ezclasslists
ActiveAccessExtensions[]=eztags
ActiveAccessExtensions[]=ocembed
ActiveAccessExtensions[]=ocrecaptcha
ActiveAccessExtensions[]=ocoperatorscollection
ActiveAccessExtensions[]=ocbootstrap
ActiveAccessExtensions[]=ocsocialuser
ActiveAccessExtensions[]=ocsocialdesign
ActiveAccessExtensions[]=ezstarrating
ActiveAccessExtensions[]=openpa_agenda
ActiveAccessExtensions[]=oceditorialstuff
ActiveAccessExtensions[]=ngopengraph
ActiveAccessExtensions[]=ngpush
ActiveAccessExtensions[]=ocbinarynullparser
ActiveAccessExtensions[]=ocmultibinary
ActiveAccessExtensions[]=mugoobjectrelations
ActiveAccessExtensions[]=ocoperatorscollection
ActiveAccessExtensions[]=ocfoshttpcache
ActiveAccessExtensions[]=easyvocs_connector
ActiveAccessExtensions[]=ocsupport
ActiveAccessExtensions[]=ezuserformtoken
ActiveAccessExtensions[]=ocgdprtools
ActiveAccessExtensions[]=ezmbpaex
ActiveAccessExtensions[]=ocevents
ActiveAccessExtensions[]=ocopendata_forms
ActiveAccessExtensions[]=openpa_bootstrapitalia
ActiveAccessExtensions[]=ocwebhookserver

[DatabaseSettings]
DatabaseImplementation=ezpostgresql
Server=postgres
Port=
User=openpa
Password=openp4ssword
Database=openagenda_prototipo
Charset=utf-8
Socket=disabled
SQLOutput=disabled

[Session]
SessionNamePerSiteAccess=disabled

[SiteSettings]
SiteName=OpenAgenda
SiteURL=openagenda.localtest.me
LoginPage=embedded
AdditionalLoginFormActionURL=http://openagenda.localtest.me/backend/user/login

[SiteAccessSettings]
RequireUserLogin=false
ShowHiddenNodes=false
RelatedSiteAccessList[]
RelatedSiteAccessList[]=prototipo_backend
RelatedSiteAccessList[]=prototipo_agenda
RelatedSiteAccessList[]=prototipo_ita_agenda

[DesignSettings]
SiteDesign=agenda_bootstrapitalia
AdditionalSiteDesignList[]
AdditionalSiteDesignList[]=bootstrapitalia
AdditionalSiteDesignList[]=agenda
AdditionalSiteDesignList[]=ocbootstrap4
AdditionalSiteDesignList[]=ocbootstrap
AdditionalSiteDesignList[]=standard

[RegionalSettings]
Locale=eng-GB
ContentObjectLocale=eng-GB
ShowUntranslatedObjects=disabled
SiteLanguageList[]
SiteLanguageList[]=eng-GB
SiteLanguageList[]=ita-IT
TextTranslation=enabled
TranslationSA[prototipo_ita_agenda]=Ita
TranslationSA[prototipo_agenda]=Eng
LanguageSA[eng-GB]=prototipo_agenda
LanguageSA[ita-IT]=prototipo_ita_agenda

[FileSettings]
VarDir=var/prototipo

[MailSettings]
AdminEmail=webmaster@opencontent.it
EmailSender=

[InformationCollectionSettings]
EmailReceiver=

[UserSettings]
RegistrationEmail=

[ContentSettings]
TranslationList=

[SiteAccessRules]
Rules[]
Rules[]=access;enable
Rules[]=moduleall
Rules[]=access;disable
Rules[]=module;ezinfo/about
Rules[]=module;setup/extensions
Rules[]=module;content/tipafriend
Rules[]=module;settings/edit
*/ ?>
