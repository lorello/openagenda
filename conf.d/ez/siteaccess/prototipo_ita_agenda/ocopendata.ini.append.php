<?php /* #?ini charset="utf-8"?

[EnvironmentSettingsPresets]
AvailablePresets[]=calendar
AvailablePresets[]=useraware

[EnvironmentSettingsPresets_calendar]
CheckAccess=false
Debug=enabled
PHPClass=OpenPABootstrapItaliaCalendarEnvironmentSettings

[EnvironmentSettingsPresets_content]
PHPClass=OpenPABootstrapItaliaContentEnvironmentSettings

[EnvironmentSettingsPresets_useraware]
PHPClass=OpenPABootstrapItaliaUserAwareEnvironmentSettings

[EnvironmentSettingsPresets_datatable]
PHPClass=OpenPABootstrapItaliaDatatableEnvironmentSettings

[AttributeConverters]
Converters[has_temporal_coverage]=FullRelationsAttributeConverter
Converters[opening_hours_specification]=FullRelationsAttributeConverter
*/ ?>