<?php /* #?ini charset="utf-8"?

[HTTPHeaderSettings]
CustomHeader=enabled
OnlyForAnonymous=disabled
OnlyForContent=enabled
Cache-Control[]
Cache-Control[/]=public, must-revalidate, max-age=259200, s-maxage=259200
HeaderList[]=Vary
Vary[/]=X-User-Context-Hash
HeaderList[]=X-Instance
X-Instance[/]=prototipo

[DatabaseSettings]
DatabaseImplementation=ezpostgresql
Server=postgres
Port=
User=openpa
Password=openp4ssword
Database=openagenda_prototipo
Charset=utf-8
Socket=disabled
SQLOutput=disabled

[SiteSettings]
SiteName=OpenCity
SiteURL=openagenda.localtest.me/backend
DefaultPage=content/dashboard
LoginPage=custom

[SiteAccessSettings]
RequireUserLogin=true
ShowHiddenNodes=true
RelatedSiteAccessList[]
RelatedSiteAccessList[]=prototipo_backend
RelatedSiteAccessList[]=prototipo_agenda
RelatedSiteAccessList[]=prototipo_ita_agenda

[DesignSettings]
SiteDesign=backend
AdditionalSiteDesignList[]
AdditionalSiteDesignList[]=admin2
AdditionalSiteDesignList[]=admin
AdditionalSiteDesignList[]=ezflow
AdditionalSiteDesignList[]=standard

[RegionalSettings]
Locale=ita-IT
ContentObjectLocale=ita-IT
ShowUntranslatedObjects=enabled
SiteLanguageList[]=ita-IT
TextTranslation=enabled

[FileSettings]
VarDir=var/prototipo

[ContentSettings]
CachedViewPreferences[full]=admin_navigation_content=1;admin_children_viewmode=list;admin_list_limit=1
TranslationList=

[MailSettings]
AdminEmail=webmaster@opencontent.it
EmailSender=

[UserSettings]
RegistrationEmail=

[InformationCollectionSettings]
EmailReceiver=

[ExtensionSettings]
ActiveAccessExtensions[]
ActiveAccessExtensions[]=ezflow
ActiveAccessExtensions[]=ezgmaplocation
ActiveAccessExtensions[]=ezjscore
ActiveAccessExtensions[]=ezmultiupload
ActiveAccessExtensions[]=ezodf
ActiveAccessExtensions[]=ezoe
ActiveAccessExtensions[]=ezwt
ActiveAccessExtensions[]=sqliimport
ActiveAccessExtensions[]=openpa
ActiveAccessExtensions[]=ezfind
ActiveAccessExtensions[]=ocsearchtools
ActiveAccessExtensions[]=ezprestapiprovider
ActiveAccessExtensions[]=ocopendata
ActiveAccessExtensions[]=ocexportas
ActiveAccessExtensions[]=ezclasslists
ActiveAccessExtensions[]=eztags
ActiveAccessExtensions[]=ocembed
ActiveAccessExtensions[]=ocrecaptcha
ActiveAccessExtensions[]=ocoperatorscollection
ActiveAccessExtensions[]=ocbootstrap
ActiveAccessExtensions[]=ocsocialuser
ActiveAccessExtensions[]=ocsocialdesign
ActiveAccessExtensions[]=openpa_agenda
ActiveAccessExtensions[]=oceditorialstuff
ActiveAccessExtensions[]=ngopengraph
ActiveAccessExtensions[]=ngpush
ActiveAccessExtensions[]=ocbinarynullparser
ActiveAccessExtensions[]=ezstarrating
ActiveAccessExtensions[]=ocmultibinary
ActiveAccessExtensions[]=ocoperatorscollection
ActiveAccessExtensions[]=ocfoshttpcache
ActiveAccessExtensions[]=easyvocs_connector
ActiveAccessExtensions[]=ocsupport
ActiveAccessExtensions[]=ezuserformtoken
ActiveAccessExtensions[]=ocgdprtools
ActiveAccessExtensions[]=ocevents
ActiveAccessExtensions[]=ezmbpaex
ActiveAccessExtensions[]=openpa_bootstrapitalia
ActiveAccessExtensions[]=ocmaintenance
ActiveAccessExtensions[]=ocwebhookserver

[SiteAccessRules]
Rules[]
Rules[]=access;enable
Rules[]=moduleall
Rules[]=access;disable
Rules[]=module;ezinfo/about
Rules[]=module;setup/extensions
Rules[]=module;content/tipafriend
Rules[]=module;settings/edit
Rules[]=module;user/register
*/ ?>